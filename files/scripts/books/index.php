<?php

require "helpers.php";

function import($database, $SesClient)
{
    if (!isset($_GET['action'])) {
        return false;
    }

    switch ($_GET['action']) {
        case "base":
            $file = "i_base.php";
            break;
        case "owned":
            $file = "i_owned.php";
            break;
        case "fixorder":
            $file = "i_fixOrder.php";
            break;
        case "count":
            $file = "i_count.php";
            break;
        default:
            return false;
    }

    require $file;
    subImport($database, $SesClient);
}