<?php

function subImport($database, $SesClient)
{
    $books = $database->query(
        "SELECT `id`, `goodreads_id`, `isbn10`, `title` FROM <books>  WHERE `isbn10` <> 0 AND `goodreads_id` = NULL", []
    )->fetchAll();

    $availableImages = getExistingImagesAll($SesClient);

    foreach ($books as $bookBad) {

        $book = [
            'id' => $bookBad['id'],
            'goodreads_id' => $bookBad['goodreads_id'],
            'isbn10' => $bookBad['isbn10'],
            'title' => $bookBad['title'],
        ];

        $bookData = null;

        if (!empty($bookBad['goodreads_id'])) {
            $bookData = getXML('https://www.goodreads.com/book/show/' . $book['goodreads_id'] . '.xml?key=' . getenv('GOODREADS_KEY'));
        } elseif ($bookBad['isbn10']) {
            $bookData = getXML('https://www.goodreads.com/book/isbn/' . $book['isbn10'] . '.xml?key=' . getenv('GOODREADS_KEY'));
        }

        if ($bookData) {

            $bookData = $bookData->book;
            $bookId = $bookData->id->__toString();

            if ($bookData->isbn->__toString() !== "") {
                $bookISBN = $bookData->isbn->__toString();
            } elseif ($bookData->isbn13->__toString() !== "") {
                $bookISBN = substr($bookData->isbn->__toString(), 2);
                logStatus('ISBN 13:' . substr($bookData->isbn->__toString(), 2), 'debug');
            } else {
                $bookISBN = false;
            }

            $xmlWorkId = file_get_contents('https://www.goodreads.com/book/id_to_work_id/' . $bookId . '?key=' . getenv('GOODREADS_KEY'));
            $docWorkId = new SimpleXMLElement($xmlWorkId);

            $book['goodreads_work_id'] = $docWorkId->{'work-ids'}->item->__toString();

            $book['goodreads_id'] = intval($bookId);
            $book['avg_rating'] = floatval($bookData->average_rating->__toString());
            $book['description'] = $bookData->description->__toString();
            if ($bookISBN) {
                $book['isbn10'] = $bookISBN;
            }

            $book['title'] = $bookData->title->__toString();

            if ($bookISBN) {
                logStatus("Import authors: " . $book['id'] . " - " . $book['title'], 'process');
                foreach ($bookData->authors->author as $author) {
                    addAuthor($author, $book, $database, $SesClient, $availableImages);
                }

                logStatus("Import series: " . $book['id'] . " - " . $book['title'], 'process');
                addSeries($book, $database);
            }

            if (!in_array('gallery/books/covers/' . $book['id'] . '.jpg', $availableImages['covers'])) {
                getImageOwned($book, $bookData, $database, $SesClient);
            }

            $database->update('books', $book, ['id' => $book['id']]);
        }
    }
}