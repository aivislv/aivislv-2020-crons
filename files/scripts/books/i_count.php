<?php

function subImport($database)
{
    $authors = $database->select('book_authors', ['id', 'goodreads_id']);

    foreach ($authors as $author) {
        countAuthor($database, $author);
    }

    $series = $database->select('book_series', ['id', 'goodreads_id']);

    foreach ($series as $serie) {
        countSeries($database, $serie);
    }
}