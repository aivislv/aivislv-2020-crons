<?php

function subImport($database, $SesClient)
{
    $userId = '4293177';
    $perpage = '200';
    $page = 1;

    $listTotal = 1;
    $listEnd = 0;

    $availableImages = getExistingImagesAll($SesClient);

    while ($listTotal > $listEnd || $page === 2) {
        $xml = file_get_contents('https://www.goodreads.com/review/list/' . $userId . '.xml?key=' . getenv('GOODREADS_KEY') . '&per_page=' . $perpage . '&page=' . $page . '&v=2');
        $doc = new SimpleXMLElement($xml);
        $listTotal = $doc->reviews['total']->__toString();
        $listEnd = $doc->reviews['end']->__toString();

        foreach ($doc->reviews->review as $review) {
            $bookId = $review->book->id->__toString();

            if ($review->book->isbn->__toString() !== "") {
                $bookISBN = $review->book->isbn->__toString();
            } elseif ($review->book->isbn13->__toString() !== "") {
                $bookISBN = substr($review->book->isbn->__toString(), 2);
            } else {
                $bookISBN = false;
            }

            $book = $database->get('books', ['id', 'goodreads_id', 'isbn10', 'goodreads_work_id', 'title'], ['goodreads_id' => intval($bookId)]);

            $found = FOUND_AS_GRID;

            if (!$book && $bookISBN) {
                $book = $database->get('books', ['id', 'goodreads_id', 'goodreads_work_id', 'isbn10', 'title'], ['isbn10' => intval($bookISBN)]);
                $found = FOUND_AS_ISBN;
            }

            /*
            if (!$book) {
                $title = trim($review->book->title_without_series->__toString());

                $book = $database->get('books', ['id', 'goodreads_id','goodreads_work_id', 'isbn10'], ['title'=>$title]);
                $found = FOUND_AS_TITLE;
            }
            */

            if (!$book) {
                $book = [];
                $found = FOUND_AS_NONE;
            }

            if ($found === FOUND_AS_NONE) {
                if ($bookISBN) {
                    $book['isbn10'] = intval($bookISBN);
                } else {
                    echo "No ISBN - Book:" . $review->book->title->__toString();
                }

                $book['title'] = trim($review->book->title_without_series->__toString());
            }

            if ($found === FOUND_AS_NONE || $found === FOUND_AS_ISBN || $found === FOUND_AS_TITLE) {
                $xmlWorkId = file_get_contents('https://www.goodreads.com/book/id_to_work_id/' . $bookId . '?key=' . getenv('GOODREADS_KEY'));
                $docWorkId = new SimpleXMLElement($xmlWorkId);

                $book['goodreads_work_id'] = $docWorkId->{'work-ids'}->item->__toString();

                $book['goodreads_id'] = intval($bookId);
                $book['avg_rating'] = floatval($review->book->average_rating->__toString());
                $book['description'] = $review->book->description->__toString();
                $book['isOwned'] = 0;
            }

            $forceCover = false;

            if ($found === FOUND_AS_GRID && $bookISBN !== false) {
                if ($book['isbn10'] != $bookISBN) {
                    $book['isbn10'] = $bookISBN;
                    $forceCover = true;
                }
            }

            // RATING
            $rating = $review->rating->__toString();
            if ($rating) {
                $book['rating'] = intval($rating);
            }

            // SHELVE

            $book['isRead'] = findAcceptedShelve($review->shelves);

            // SAVE CURRENT STATE

            if ($found === FOUND_AS_NONE) {
                $database->insert('books', $book);

                $book['id'] = $database->id();
            } else {
                $database->update('books', $book, ['id' => $book['id']]);
            }

            // AUTHOR (IF NOT GRID)

            if ($found === FOUND_AS_NONE || $found === FOUND_AS_ISBN || $found === FOUND_AS_TITLE) {
                logStatus("Import authors: " . $book['id'] . " - " . $book['title'], 'process');
                foreach ($review->book->authors->author as $author) {
                    addAuthor($author, $book, $database, $SesClient, $availableImages);
                }
            }

            // SERIES (IF NOT GRID)

            if ($found === FOUND_AS_NONE || $found === FOUND_AS_ISBN || $found === FOUND_AS_TITLE) {
                logStatus("Import series: " . $book['id'] . " - " . $book['title'], 'process');
                addSeries($book, $database);
            }

            // IMAGE (IF NOT GRID)

            if (!in_array('gallery/books/covers/' . $book['id'] . '.jpg', $availableImages['covers'])) {
                $forceCover = true;
                logStatus("Image missing: " . $book['id'] . " - " . $book['title'], 'update');
            } else {
                logStatus("Image found: " . $book['id'] . " - " . $book['title'], 'process');
            }

            if ($found === FOUND_AS_NONE || $found === FOUND_AS_ISBN || $found === FOUND_AS_TITLE || check("reuploadImages", "force") || $forceCover) {
                getImage($book, $review, $database, $SesClient);
            }
        }

        $page++;
    }
}