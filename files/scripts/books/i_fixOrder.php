<?php

function subImport($database)
{
    $books = $database->select('books', ['id', 'goodreads_id', 'goodreads_work_id']);

    foreach ($books as $book) {
        if ($book['goodreads_work_id']) {
            addSeries($book, $database);
        }
    }
}