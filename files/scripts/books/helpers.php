<?php

define("FOUND_AS_ISBN", 'FOUND_AS_ISBN');
define("FOUND_AS_GRID", 'FOUND_AS_GRID');
define("FOUND_AS_NONE", 'FOUND_AS_NONE');
define("FOUND_AS_TITLE", 'FOUND_AS_TITLE');

function countAuthor($database, $author)
{
    $count = $database->count('book2author', ['author_id' => $author['id']]);

    $database->update('book_authors', ['books_linked' => $count], ['id' => $author['id']]);
}

function countSeries($database, $series)
{
    $count = $database->count('book2series', ['series_id' => $series['id']]);

    $database->update('book_series', ['books_linked' => $count], ['id' => $series['id']]);
}

function findAcceptedShelve($list)
{
    $sheveIds = ['to-read' => 0, 'currently-reading' => 1, 'read' => 2];

    foreach ($list->shelf as $item) {
        if (isset($sheveIds[$item['name']->__toString()])) {
            return $sheveIds[$item['name']->__toString()];
        }
    }

    return null;
}

function addAuthor($authorData, $bookModel, $database, $SesClient, $availableImages)
{
    $author = $database->get('book_authors', ['id', 'goodreads_id'], ['goodreads_id' => intval($authorData->id->__toString())]);

    if (!$author) {
        $author = [
            'goodreads_id' => intval($authorData->id->__toString()),
            'name_eng' => trim($authorData->name->__toString())
        ];

        $database->insert(
            'book_authors',
            $author
        );

        $author['id'] = $database->id();

        logStatus("Adding author: " . $author['id'] . " - " . $author['name'], 'update');
    }

    if (!in_array('gallery/books/authors/' . $author['id'] . '.jpg', $availableImages['authors'])) {
        getAuthorImage($author, $authorData, $SesClient);
    }

    linkAuthor2Book($author, $bookModel, $database);

    countAuthor($database, $author);
}

function linkAuthor2Book($author, $book, $database)
{
    $link = $database->get('book2author', ['book_id'], ['book_id' => intval($book['id']), 'author_id' => intval($author['id'])]);

    if (!$link) {
        $database->insert('book2author',
            [
                'book_id' => intval($book['id']),
                'author_id' => intval($author['id']),
            ]
        );
    }
}

function addSeries($book, $database)
{
    $docSeries = getXML('https://www.goodreads.com/work/' . $book['goodreads_work_id'] . '/series?format=xml&key=' . getenv('GOODREADS_KEY'));
    if (!$docSeries) {
        return false;
    }

    foreach ($docSeries->series_works->series_work as $seriesData) {
        $seriesId = $seriesData->series->id->__toString();

        $series = $database->get('book_series', ['id, goodreads_id'], ['goodreads_id' => intval($seriesId)]);

        if (!$series) {
            $series = [
                'goodreads_id' => $seriesId,
                'title' => trim($seriesData->series->title->__toString()),
            ];

            $database->insert(
                'book_series',
                $series
            );

            $series['id'] = $database->id();

            logStatus("Adding series: " . $series['id'] . " - " . $series['title'], 'update');
        }

        linkSeries2Book($series, $seriesData, $book, $database);

        countSeries($database, $series);
    }
}

function linkSeries2Book($series, $seriesData, $book, $database)
{
    $link = $database->get('book2series', ['book_id'], ['book_id' => intval($book['id']), 'series_id' => intval($series['id'])]);

    if (!$link) {
        $position = $seriesData->user_position->__toString();
        $positionLast = null;

        if (floatval($position) != $position) {
            if (stripos($position, '-') !== false) {
                $parts = explode('-', $position);
                $position = trim($parts[0]);
                $positionLast = trim($parts[count($parts) - 1]);
            }
        }

        $database->insert('book2series',
            [
                'book_id' => intval($book['id']),
                'series_id' => intval($series['id']),
                'ordered' => floatval($position),
                'last_ordered' => $positionLast,
            ]
        );
    } else {
        $position = $seriesData->user_position->__toString();
        $positionLast = null;

        if ($position != $link['ordered']) {
            if (stripos($position, '-') !== false) {
                $parts = explode('-', $position);
                $position = trim($parts[0]);
                $positionLast = trim($parts[count($parts) - 1]);
            }
        }

        $database->update('book2series',
            [
                'ordered' => floatval($position),
                'last_ordered' => floatval($positionLast),
            ],
            [
                'book_id' => intval($book['id']),
                'series_id' => intval($series['id']),
            ]
        );
    }
}

function getImageOwned($book, $bookData, $database, $SesClient)
{
    $image_link = $bookData->image_url->__toString();

    $hasNoPhoto = stripos($image_link, 'nophoto');

    if ($hasNoPhoto === false) {
        $resultLink = trim(str_ireplace('_SX98_.', '', $image_link));
    } else {
        return false;
    }

    downloadImage($SesClient, $resultLink, $book['id'], 'book', 'gallery/books/covers/', $book['title'], 250);
}

function getFromGoogle($book)
{
    $curl = curl_init('https://www.googleapis.com/books/v1/volumes?q=' . rawurlencode($book['title']));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($curl);

    $resultEx = json_decode($result, true);

    if ($resultEx['totalItems'] > 0) {
        logStatus("Google Image found for : " . $book['id'] . " - " . $book['title'], 'update');

        return $resultEx['items'][0]['volumeInfo']['imageLinks']['thumbnail'];
    }

    return null;
}

function getImage($book, $reviewData, $database, $SesClient)
{
    $image_link = $reviewData->book->image_url->__toString();

    $hasNoPhoto = stripos($image_link, 'nophoto');

    if ($hasNoPhoto === false) {
        logStatus("Image found for :" . $book['id'] . " - " . $book['title'], 'process');
        $resultLink = trim(str_ireplace('_SX98_.', '', $image_link));
    } else {
        logStatus("No image found for :" . $book['id'] . " - " . $book['title'], 'error');
        $resultLink = getFromGoogle($book);
        if (!$resultLink) {
            return null;
        }
    }

    downloadImage($SesClient, $resultLink, $book['id'], 'book', 'gallery/books/covers/', $book['title'], 250);
}

function getAuthorImage($author, $authorData, $SesClient)
{
    $image_link = $authorData->image_url->__toString();
    $hasNoPhoto = $authorData->image_url['nophoto']->__toString();

    logStatus("Adding author photo: " . $author['id'] . " - " . $author['name'], 'process');

    if ($hasNoPhoto == "false") {
        $resultLink = trim(str_ireplace('p5', 'p7', $image_link));
    } else {
        logStatus("Author photo not found: " . $author['id'] . " - " . $author['name'], 'process');
        return false;
    }

    downloadImage($SesClient, $resultLink, $author['id'], 'author', 'gallery/books/authors/', $author['name'], 250);
}

function getXML($link)
{
    $curl = curl_init($link);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($curl);
    if (!curl_errno($curl)) {
        $info = curl_getinfo($curl);
    } else {
        var_dump(curl_errno($curl));
    }

    if ($info['http_code'] == 200) {
        return new SimpleXMLElement($result);
    } else {
        return false;
    }
}

function getExistingImagesAll($SesClient)
{
    return [
        'authors' => getExistingImages($SesClient, 'gallery/books/authors/'),
        'covers' => getExistingImages($SesClient, 'gallery/books/covers/')
    ];
}