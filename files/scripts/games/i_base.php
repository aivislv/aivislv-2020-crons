<?php

function subImport($database, $SesClient)
{
    $user = 'aivislisovskis';
    $all = false;

    if (check('refetchPlayerCount', "force")) {
        importAllPlayerCounts($database);
        exit();
    }

    $xml_link = 'https://www.boardgamegeek.com/xmlapi2/collection?username=' . $user . '&stats=1&own=1';
    $presetGameType = 1;

    if (isset($_GET['list'])) {
        switch ($_GET['list']) {
            case "wish":
                $xml_link = 'https://www.boardgamegeek.com/xmlapi2/collection?username=' . $user . '&stats=1&wishlist=1';
                $presetGameType = 2;
                break;
            case "played":
                $xml_link = 'https://www.boardgamegeek.com/xmlapi2/collection?username=' . $user . '&stats=1&wishlist=0&played=1&own=0';
                $presetGameType = 3;
                break;
            case "accessories":
                $xml_link = 'https://www.boardgamegeek.com/xmlapi2/collection?username=' . $user . '&stats=1&subtype=boardgameaccessory';
                break;
            default:
                break;
        }
    }

    logStatus($xml_link, 'debug');

    $xml = file_get_contents($xml_link);

    logStatus("Start parsing XML", 'debug');

    $doc = new SimpleXMLElement($xml);

    $availableImages = getExistingImages($SesClient, 'gallery/games/');

    for ($a = 0; $a < $doc['totalitems']; $a++) {
        $item = $doc->item[$a];

        $game = $database->get('games', ['id', 'bgg_id'], ['bgg_collid' => intval($item['collid'])]);

        if (!$game) {
            logStatus("Adding: <strong>" . $item->name->__toString() . "</strong>", 'update');
            $game = [];

            $game['bgg_id'] = $item['objectid']->__toString();
            $game['bgg_collid'] = $item['collid']->__toString();
            $game['plays'] = $item->numplays->__toString();
            $game['rating'] = 0;
            $game['expansion'] = 0;
            $game['title'] = $item->name->__toString();

            if (isset($item->stats['minplayers'])) {
                $game['min_players'] = $item->stats['minplayers']->__toString();
            }
            if (isset($item->stats['maxplayers'])) {
                $game['max_players'] = $item->stats['maxplayers']->__toString();
            }
            if (isset($item->stats['minplaytime'])) {
                $game['min_playtime'] = $item->stats['minplaytime']->__toString();
            }
            if (isset($item->stats['maxplaytime'])) {
                $game['max_playtime'] = $item->stats['maxplaytime']->__toString();
            }
            if (isset($item->stats['playingtime'])) {
                $game['playtime'] = $item->stats['playingtime']->__toString();
            }

            if (isset($item->stats->rating->average['value'])) {
                $game['avg_score'] = $item->stats->rating->average['value']->__toString();
            }

            if (isset($item->stats->rating->bayesaverage['value'])) {
                $game['bye_score'] = $item->stats->rating->bayesaverage['value']->__toString();
            }

            if (isset($item->stats->rating->usersrated['value'])) {
                $game['usercount'] = $item->stats->rating->usersrated['value']->__toString();
            }

            if (isset($item->status['wishlistpriority'])) {
                $game['wishlist_priority'] = $item->status['wishlistpriority']->__toString();
            }

            if (isset($item->status['preordered'])) {
                $game['preordered'] = $item->status['preordered']->__toString();
            }

            $game['rank'] = intval($item->stats->rating->ranks->rank[0]['value']->__toString());

            if (check('accessories', 'list')) {
                if ($item->status['own'] == "1") {
                    $presetGameType = 1;
                }

                if ($item->status['wishlist'] == "1") {
                    $presetGameType = 2;
                }
            }

            $game['type'] = $presetGameType;

            $game = expansion($game, $database);

            if (check('accessories', "list")) {
                $game['is_accessory'] = 1;
            }

            $database->insert('games', [
                'bgg_id' => intval($game['bgg_id']),
                'plays' => intval($game['plays']),
                'min_players' => intval($game['min_players']),
                'max_players' => intval($game['max_players']),
                'min_playtime' => intval($game['min_playtime']),
                'max_playtime' => intval($game['max_playtime']),
                'playtime' => intval($game['playtime']),
                'usercount' => intval($game['usercount']),
                'rank' => intval($game['rank']),
                'avg_score' => floatval($game['playtime']),
                'bye_score' => floatval($game['bye_score']),
                'type' => $game['type'],
                'title' => $game['title'],
                'rating' => $game['rating'],
                'expansion' => intval($game['expansion']),
                'description' => $game['description'],
                'group_parent_id' => intval($game['group_parent_id']),
                'is_accessory' => intval($game['is_accessory']),
                'preordered' => intval($game['preordered']),
                'bgg_collid' => intval($game['bgg_collid']),
                'wishlist_priority' => intval($game['wishlist_priority']),
            ]);

            $game['id'] = $database->id();

            var_dump($database->error());

            $g = true;

        } else {
            logStatus("Updating: <strong>" . $item->name->__toString() . "</strong>", 'process');
            $game['rating'] = floatval($item->stats->rating['value']->__toString());

            $game['bgg_collid'] = $item['collid']->__toString();

            $game['title'] = $item->name->__toString();

            $game['plays'] = $item->numplays->__toString();

            if (isset($item->stats->rating->average['value'])) {
                $game['avg_score'] = $item->stats->rating->average['value']->__toString();
            }

            if (isset($item->stats->rating->bayesaverage['value'])) {
                $game['bye_score'] = $item->stats->rating->bayesaverage['value']->__toString();
            }

            if (isset($item->stats->rating->usersrated['value'])) {
                $game['usercount'] = $item->stats->rating->usersrated['value']->__toString();
            }

            if (isset($item->status['wishlistpriority'])) {
                $game['wishlist_priority'] = $item->status['wishlistpriority']->__toString();
            }

            if (isset($item->status['preordered'])) {
                $game['preordered'] = $item->status['preordered']->__toString();
            }

            $game['rank'] = intval($item->stats->rating->ranks->rank[0]['value']->__toString());

            if (check('accessories', 'list')) {
                if ($item->status['own'] == "1") {
                    $presetGameType = 1;
                }

                if ($item->status['wishlist'] == "1") {
                    $presetGameType = 2;
                }
            }

            $game['type'] = $presetGameType;

            // $game = expansion($game, $database);

            $database->update('games', [
                'plays' => intval($game['plays']),
                'usercount' => intval($game['usercount']),
                'rank' => intval($game['rank']),
                'avg_score' => floatval($game['playtime']),
                'bye_score' => floatval($game['bye_score']),
                'bgg_collid' => intval($game['bgg_collid']),
                'type' => $game['type'],
                'rating' => $game['rating'],
                'title' => $game['title'],
                'preordered' => intval($game['preordered']),
                'wishlist_priority' => intval($game['wishlist_priority']),
            ], ['id' => $game['id']]);

            $g = false;
        }

        $forceCover = false;

        if (!in_array('gallery/games/' . $game['id'] . '.jpg', $availableImages)) {
            $forceCover = true;
            logStatus("Missing: " . $game['id'] . " - " . $game['title'], 'update');
        } else {
            logStatus("Exists: " . $game['id'] . " - " . $game['title'], 'process');
        }

        if ($g || check("reuploadImages", "force") || $forceCover) {
            downloadImage($SesClient, $item->image, $game['id'], 'game', 'gallery/games/', $game['title'], 250);
        }

        if ($g && !check('accessories', 'list')) {
            doImportBest($database, $game);
        }

        if (isset($_GET['slow'])) {
            sleep(5);
        }
    }

    if (!check('wish', "list") && !check("skipPlays", "force") && !check('accessories', 'list')) {
        plays($database);
    }
}

function importAllPlayerCounts($database)
{
    $baseGames = $database
        ->query("SELECT `id`, `bgg_id`, `expansion`, `players` FROM `games` WHERE `expansion` = 0")
        ->fetchALl();

    foreach ($baseGames as $game) {
        if ($game['bgg_id'] > 0 && $game['expansion'] == 0) {
            doImportBest($database, $game);
        }
    }
}

function doImportBest($database, $game)
{
    $voteType = [
        "Best" => 1,
        "Recommended" => 2,
        "Not Recommended" => 3,
    ];

    libxml_use_internal_errors(true);

    try {
        $xml = file_get_contents('https://www.boardgamegeek.com/xmlapi/boardgame/' . $game['bgg_id']);

        $docs = new SimpleXMLElement($xml);
        $json = json_encode($docs);
        $array = json_decode($json, TRUE);

        $best = null;

        foreach ($array['boardgame']['poll'] as $poll) {
            if ($poll['@attributes']['name'] == "suggested_numplayers") {
                foreach ($poll['results'] as $item) {

                    $res = [];
                    foreach ($item['result'] as $result) {
                        $res[$result['@attributes']['value']] = $result['@attributes']['numvotes'];
                    }

                    $maxT = "Not Recommended";
                    $maxV = 0;

                    foreach ($res as $type => $votes) {
                        if ($votes > $maxV) {
                            $maxT = $type;
                            $maxV = $votes;
                        }
                    }

                    $best[] = [
                        'players' => $item['@attributes']['numplayers'],
                        'votes' => $maxV,
                        'status' => $voteType[$maxT],
                    ];
                }
            }
        }

        $database->update('games', ['players' => json_encode($best)], ['id' => $game['id']]);

        foreach ($best as $item) {
            $players = $database->get('games_players', ['game_id' => $game['id'], 'players' => $item['players']]);
            if ($players) {
                $players['votes'] = $item['votes'];
                $players['status'] = $item['status'];

                $database->update('games_players', $players, ['id' => $players['id']]);
            } else {
                $players = ['players' => $item['players'], 'game_id' => $game['id']];
                $players['votes'] = $item['votes'];
                $players['status'] = $item['status'];
                $database->insert('games_players', $players);
            }
        }
    } catch (Exception $e) {
        logStatus("No Data", 'error');
        var_dump($e);
    }
}

function plays($database)
{
    libxml_use_internal_errors(true);

    $page = 0;

    try {
        $hasPlays = true;
        $page++;

        $xml = file_get_contents('https://www.boardgamegeek.com/xmlapi2/plays?username=aivislisovskis' . '&page=' . $page);
        $docs = new SimpleXMLElement($xml);
        $json = json_encode($docs);
        $array = json_decode($json, TRUE);

        while (isset($array['play'])) {

            importPlays($array['play'], $database);
            logStatus("Plays page: <strong>{$page}</strong> done!", 'process');

            $page++;
            $xml = file_get_contents('https://www.boardgamegeek.com/xmlapi2/plays?username=aivislisovskis' . '&page=' . $page);
            $docs = new SimpleXMLElement($xml);
            $json = json_encode($docs);
            $array = json_decode($json, TRUE);
        }

    } catch (Exception $e) {
        logStatus("No Data", 'error');
        var_dump($e);
    }
}

function importPlays($list, $database)
{
    foreach ($list as $play) {
        $play_exists = $database->get('games_plays', ['id', 'game_id'], ['bgg_play_id' => intval($play['@attributes']['id'])]);

        if (empty($play_exists)) {

            $newPlay = [];

            $newPlay['bgg_play_id'] = intval($play['@attributes']['id']);
            $newPlay['date'] = $play['@attributes']['date'];
            $newPlay['quantity'] = intval($play['@attributes']['quantity']);
            $newPlay['game_id'] = null;

            $game = $database->get('games', ['id', 'bgg_id'], ['bgg_id' => intval($play['item']['@attributes']['objectid'])]);

            if (!empty($game)) {
                $newPlay['game_id'] = $game['id'];
            }

            $database->insert('games_plays', $newPlay);

        } else {
            if (!$play_exists['game_id']) {
                $game = $database->get('games', ['id', 'bgg_id'], ['bgg_id' => intval($play['item']['@attributes']['objectid'])]);

                if (!empty($game)) {
                    $play_exists['game_id'] = $game['id'];
                    $database->update('games_plays', $play_exists, ['id' => $play_exists['id']]);
                }
            }
        }
    }
}

function expansion($game, $database)
{
    libxml_use_internal_errors(true);

    $xml = file_get_contents('https://www.boardgamegeek.com/xmlapi2/thing?type=boardgame,boardgameexpansion,boardgameaccessory&id=' . $game['bgg_id']);

    try {
        $docs = new SimpleXMLElement($xml);
        $json = json_encode($docs);
        $array = json_decode($json, TRUE);

        $family = $game['bgg_id'];

        if (isset($array['item']) && isset($array['item']['link'])) {
            foreach ($array['item']['link'] as $b) {
                if ($b['@attributes']['type'] == 'boardgameaccessory' || $b['@attributes']['type'] == 'boardgameexpansion' || $b['@attributes']['type'] == 'boardgameintegration') {
                    if ($family > $b['@attributes']['id']) {
                        $family = $b['@attributes']['id'];
                    }
                }
            }

            $game['description'] = $array['item']['description'];
        }
        $game['group_parent_id'] = $family;

        if ($game['id'] != $game['group_parent_id']) {
            $parent = $database->get('games', ['id', 'bgg_id'], ['bgg_id' => $game['group_parent_id']]);

            if ($parent) {
                $game['expansion'] = $game['group_parent_id'];
            }
        }

        return $game;
    } catch (Exception $e) {
        logStatus("No Data", 'error');
    }
}