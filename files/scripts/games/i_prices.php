<?php
/*


BGG = https://boardgamegeek.com/xmlapi2/thing?type=boardgame,boardgameexpansion,boardgameaccessory&id=311988&marketplace=1

boardgameprices = https://boardgameprices.co.uk/api/info/?eid=311988

Ebay - https://developer.ebay.com/api-docs/buy/browse/static/overview.html
https://developer.ebay.com/api-docs/user-guides/static/finding-user-guide/finding-making-a-call.html
https://stackoverflow.com/questions/17116436/changing-ebay-api-target-site-from-us-to-uk

*/

function subImport($database, $SesClient)
{
    $filters = ["type" => 2,];

    if (!check("all", "list")) {
        $filters["check_price"] = 1;
    }

    if (isset($_GET['id'])) {
        $filters["id"] = intval($_GET['id']);
    }

    $game_list = $database->select("games", ["id", "bgg_id", "title"], $filters);

    foreach ($game_list as $game) {
        importPrices($database, $game);
        sleep(3);
    }
}

function importPrices($database, $game)
{
    importBgg($database, $game);
    importBGPrices($database, $game);
    // importEbay($database, $game);
    // exit(0);
}

function importBGG($database, $game)
{
    $source = "bgg";

    $xml = file_get_contents('https://www.boardgamegeek.com/xmlapi2/thing?type=boardgame,boardgameexpansion,boardgameaccessory&marketplace=1&id=' . $game['bgg_id']);
    $docs = new SimpleXMLElement($xml);
    $json = json_encode($docs);
    $array = json_decode($json, TRUE);

    $marketItems = $array['item']['marketplacelistings']['listing'];

    foreach ($marketItems as $item) {
        $useDate = strtotime($item['listdate']['@attributes']['value']);

        $source_id = explode("/", $item['link']['@attributes']['href'])[5];

        $exists = $database->get("game_prices", ["id"], ["source_id" => $source_id, "source" => $source]);

        if (!$exists) {
            $data = [
                'date' => date("Y-m-d H:i:s", $useDate),
                'title' => $game['title'],
                'item_id' => $game['id'],
                'source_id' => $source_id,
                'source' => $source,
                'link' => $item['link']['@attributes']['href'],
                'price' => $item['price']['@attributes']['value'],
                'currency' => $item['price']['@attributes']['currency'],
                'condition' => $item['condition']['@attributes']['value'],
                'details' => $item['notes']['@attributes']['value'],
            ];

            $database->insert('game_prices', $data);
        }

    }

}

function importBGPrices($database, $game)
{
    // https://boardgameprices.co.uk/api/info/?eid=311988
    $source = "bgp";
    $currency = "EUR";
    $time = date("Y-m-d H:i:s", time());

    $xml = file_get_contents('https://boardgameprices.co.uk/api/info/?eid=' . $game['bgg_id']);
    $data = json_decode($xml, true);

    foreach ($data['items'] as $item) {
        if ($item['versions']['lang'][0] == "GB") {
            foreach ($item['prices'] as $price) {
                if ($price['stock'] === "Y") {
                    $source_id = explode("storeitemid=", $price['link'])[1];

                    $exists = $database->get("game_prices", ["id"], ["source_id" => $source_id, "source" => $source]);

                    if (!$exists) {
                        $data = [
                            'date' => $time,
                            'title' => $game['title'],
                            'item_id' => $game['id'],
                            'source_id' => $source_id,
                            'source' => $source,
                            'link' => $price['link'],
                            'price' => $price['product'],
                            'shipping' => floatval($price['shipping']),
                            'currency' => $currency,
                            'condition' => "new",
                            'details' => "",
                            'country' => $price['country'],
                        ];

                        $database->insert('game_prices', $data);
                    }
                }
            }
        }
    }
}

function importEbay()
{
    $source = "ebay";
}