<?php

function import($database, $SesClient)
{
    if (!isset($_GET['action'])) {
        return false;
    }

    switch ($_GET['action']) {
        case "base":
            $file = "i_base.php";
            break;
        case "expansions":
            $file = "i_expansions.php";
            break;
        case "prices":
            $file = "i_prices.php";
            break;
        default:
            return false;
    }

    require $file;
    subImport($database, $SesClient);
}