<?php

function import()
{
    ?>
    <div>
        <h2>Help:</h2>
        <h3>action</h3>
        <div>
            base - get game list
        </div>
        <div>
            expansions - get newest expansion info
        </div>
        <div>
            prices - get wishlist price update
        </div>
        <h3>Base</h3>
        <h4>force</h4>
        <div>
            refetchPlayerCount - refetch played game counts
        </div>
        <div>
            reuploadImages - fixing images (forcibly reupload all images)
        </div>
        <div>
            skipPlays - skip loading plays (wish/accessories default)
        </div>
        <h4>slow</h4>
        <div>
            no value - 5 second timeout between each game import (prevents BGG server responding with "Too Many
            requests" on
            large dataset)
        </div>
        <h4>list</h4>
        <div>
            played - load played game list
        </div>
        <div>
            wish - load wishlist
        </div>
        <div>
            accessories - load accessories
        </div>

        <h3>Expansions</h3>
        <h4>list</h4>
        <div>
            all - do not a
        </div>
        <h4>id</h4>
        <div>
            [number] - id for board game - get specific game updates
        </div>

        <h3>Prices</h3>
        <h4>list</h4>
        <div>
            all - do not a
        </div>
        <h4>id</h4>
        <div>
            [number] - id for board game - get specific game updates
        </div>

    </div>
<?php } ?>