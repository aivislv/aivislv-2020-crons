<?php

function import()
{
    $memcache_obj = new Memcache;
    $memcache_obj->connect(getenv('MEMC_HOST'), getenv('MEMC_PORT'));

    $memcache_obj->flush();
}