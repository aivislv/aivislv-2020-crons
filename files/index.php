<?php
require "helpers/header.php";

require 'helpers/init.php';

if ($_GET['do']) {
    switch ($_GET['do']) {
        case "books":
        {
            $file = "scripts/books/";
            $title = "Books";
            break;
        }
        case "games":
        {
            $file = "scripts/games/";
            $title = "Games";
            break;
        }
        case "anime":
        {
            $file = "scripts/anime/";
            $title = "Anime";
            break;
        }
        case "movies":
        {
            $file = "scripts/movies/";
            $title = "Movies";
            break;
        }
        case "flush":
        {
            $file = "scripts/service/flush.php";
            $absolute = true;
            $title = "Flush MC";
            break;
        }
        case "help":
        {
            $file = "scripts/service/help.php";
            $absolute = true;
            $title = "Full help";
            break;
        }
        default:
        {
            $file = false;
        }
    }

    try {
        if ($file) {
            if (isset($absolute)) {
                require $file;
            } else {
                require $file . (isset($_GET['help']) ? "help.php" : "index.php");
            }

            preventParallel($database, $title . "-" . $_GET['action']);
            import($database, $SesClient);
            finishParallel($database, $title . "-" . $_GET['action']);

            if (isset($_GET['log'])) {
                var_dump($database->log());
            }
        } else {
            echo 'No process selected';
        }
    } catch (Exception $e) {
        if (isset($_GET['log'])) {
            var_dump($database->log());
        }
        echo "Failed with: ", $e->getMessage(), "\n";
    }
}

require "helpers/footer.php";
