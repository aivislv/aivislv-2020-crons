<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-language" content="en"/>
    <meta content="initial-scale=1, maximum-scale=1 shrink-to-fit=no, user-scalable=no" name="viewport"/>
    <meta content="ie=edge" http-equiv="x-ua-compatible"/>
    <title>Aivis crons</title>
</head>
<article>
    <a href="<?= getenv('ADMIN_HOST'); ?>/crons">Back to Import</a>

    <h2>
        Execute: <?= $_GET['do'] . (isset($_GET['do']) ? (" - " . $_GET['action']) : "") . (isset($_GET['list']) ? (" - " . $_GET['list']) : "") ?></h2>

    <h4>To work</h4>
    <ul>
        By default all files do dry run.
        <li>log - all/update/process - show logs (default is update)</li>
        <li>debug - execute && show logs even if feature disabled in admin</li>
        <li>help - show available settings</li>
    </ul>
</article>
