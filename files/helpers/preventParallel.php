<?php

/**
 * @throws Exception
 */
function preventParallel($database, $process = "")
{
    if (isset($_GET['ignoreSafety'])) {
        return true;
    }
    $offset = 10 * 60;

    $processEntry = $database->get('prevent_parallel_crons', ['timestamp'], ['name' => $process]);

    if (!$processEntry) {
        $database->insert("prevent_parallel_crons", ['name' => $process, 'timestamp' => time()]);
        return true;
    }

    if ($processEntry['timestamp'] + $offset > time()) {
        throw new Exception("Process '" . $process . "' already is running!");
    }

    $database->update("prevent_parallel_crons", ['timestamp' => time()], ['name' => $process]);

    return true;
}

function finishParallel($database, $process = "")
{
    $database->update("prevent_parallel_crons", ['timestamp' => 0], ['name' => $process]);
}