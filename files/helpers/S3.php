<?php

use Aws\S3\S3Client;

require '../lib/vendor/autoload.php';

if (!isDev()) {
    $SesClient = new S3Client([
        'version' => 'latest',
        'region' => getenv('AWS_REGION'),
        'credentials' => [
            'key' => getenv('AWS_ACCESS_KEY_ID'),
            'secret' => getenv('AWS_SECRET_ACCESS_KEY'),
        ]
    ]);
} else {
    $SesClient = false;
}