<?php

use Aws\S3\S3Client;
use Medoo\Medoo;

require '../lib/vendor/autoload.php';

require "helpers.php";

require 'Medoo.php';
require "S3.php";

require 'log.php';
require "preventParallel.php";
require 'downloadImage.php';

set_time_limit(3600);
ini_set('max_execution_time', 3600);

$pdo = new PDO('mysql:host=' . getenv('MYSQL_HOST') . ';dbname=' . getenv('MYSQL_DB'), getenv('MYSQL_USER'), getenv('MYSQL_PASS'));

$database = new Medoo([
    // Initialized and connected PDO object
    'pdo' => $pdo,
    // [optional] Medoo will have different handle method according to different database type
    'database_type' => 'mysql',
    'logging' => isDev()
]);

$database->query("SET NAMES 'utf8'")->fetchAll();
