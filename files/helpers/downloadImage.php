<?php

function downloadImage($SesClient, $link, $id, $tmpfileName, $resultPath, $title, $thumbSize = 250) {
    if (isDev()) {
        return true;
    }

    try {
        logStatus("Trying to get image: " . $id . " - " . $title, 'process');

        $curl = curl_init($link);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($curl);
        if (!curl_errno($curl)) {
            $info = curl_getinfo($curl);
        } else {
            logStatus("Failed curl: " . $id . " - " . $title, 'error');
            logStatus(curl_error($curl), 'error');
        }

        curl_close($curl);

        if ($info['http_code'] == 200) {
            logStatus("Updating image: " . $id . " - " . $title, 'update');
            $im_data = $result;

            $realPath = getcwd() . '/images/' . $tmpfileName . '_tmp_orig.jpg';

            file_put_contents($realPath, $im_data);

            $imagick = new Imagick(realpath($realPath));
            $imageSizes = $imagick->getImageGeometry();
            $imageProportions = $imageSizes['width'] / $imageSizes['height'];

            if ($imageProportions > 1) {
                $newWidth = $thumbSize;
                $newHeight = round($thumbSize / $imageProportions);
            } else {
                $newWidth = round($thumbSize * $imageProportions);
                $newHeight = $thumbSize;
            }

            $newWidth = $imageSizes['width'] > $newWidth ? $newWidth : $imageSizes['width'];
            $newHeight = $imageSizes['height'] > $newHeight ? $newHeight : $imageSizes['height'];

            $imagick->resizeImage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1);

            $saveTmp = getcwd() . '/images/' . $tmpfileName . '_tmp_' . $id . '.jpg';

            $imagick->writeImage($saveTmp);

            $SesClient->putObject([
                'Bucket' => 'cdn.aivis.lv',
                'Key' => $resultPath . $id . '.jpg',
                'Body' => fopen($saveTmp, 'r'),
                'ACL' => 'public-read',
            ]);

            return true;
        } else {
            logStatus("Image not found: " . $id . " - " . $title, 'error');
        }
    } catch (Exception $e) {
        logStatus("Failed to load " . $link . ", ID: " . $id . " - " . $title, 'error');
    }
}


function getExistingImages($SesClient, $prefix)
{
    if (isDev()) {
        return [];
    }

    $availableImages = $SesClient->ListObjectsV2([
        'Bucket' => 'cdn.aivis.lv',
        'MaxKeys' => 1000,
        'Prefix' => $prefix,
    ]);

    $game = [];
    $cycle = 0;

    while (count($availableImages['Contents']) === 1000 && $cycle < 15) {

        foreach ($availableImages['Contents'] as $image) {
            $game[] = $image['Key'];
        }

        $availableImages = $SesClient->ListObjectsV2([
            'Bucket' => 'cdn.aivis.lv',
            'MaxKeys' => 1000,
            'Prefix' => $prefix,
            'StartAfter' => $availableImages['Contents'][999]['Key']
        ]);

        $cycle++;
    }

    foreach ($availableImages['Contents'] as $image) {
        $game[] = $image['Key'];
    }

    return $game;
}