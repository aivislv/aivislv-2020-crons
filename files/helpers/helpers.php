<?php

function show($time)
{
    var_dump(date("Y-m-d H:i:s", $time), $time);
}

function isDev()
{
    return getenv('ISDEV') == "1";
}

function check($action, $type = "action")
{
    if (isset($_GET[$type]) && $_GET[$type] === $action) {
        return true;
    }

    logStatus($action . "; " . $type, "process");

    return false;
}