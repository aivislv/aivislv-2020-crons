<?php
require 'init.php';

logStatus("<a href='" . getenv('ADMIN_HOST') . "/crons'>Back to Import</a>", 'title');
logStatus("<h3>Movies</h3>", 'title');
?>
<form enctype="multipart/form-data" method="post" action="movies.php<?=isset($_GET['reuploadImages']) ? '?reuploadImages' : ''?>">
    <h3><?=isset($_GET['wish']) ? 'Wishlist!' : 'Watched!';?></h3>
    <input type="hidden" value="<?=isset($_GET['wish']) ? 'wishlist' : 'ratings';?>" name="type" />
    <input type="file" name="csv" />
    <button type="submit">Upload and import!</button>
</form>
<?php

if (isset($_POST['type'])) {
    importcsv($database, $SesClient);
}

function addOrUpdate($item, $list, $database, $SesClient, $existingImages) {
    $movieData = explode(',', $item);

    if (($_POST['type']) !== 'wishlist') {
        $id = array_search($movieData[0], array_column($list, 'imdb_id'));

        if ($id !== false) {
            $movie = $list[$id];

            $movie['rating'] = intval($movieData[1]);
            $movie['seen'] = intval($movieData[2]);
            $movie['status'] = 1;
            $movie['score'] = floatval($movieData[6]);

            $g = false;

            $database->update('movies', $movie, ['id' => $movie['id']]);
        } else {
            $movie = [
                'title' => $movieData[3],
                'imdb_id' => $movieData[0],
                'rating' => intval($movieData[1]),
                'seen' => $movieData[2],
                'status' => 1,
                'score' => floatval($movieData[6]),
                'year' => intval($movieData[8]),
            ];

            $g = true;

            $database->insert('movies', $movie);
            $movie['id'] = $database->id();
        }
    } else {
        $id = array_search($movieData[1], array_column($list, 'imdb_id'));

        if ($id !== false) {
            $movie = $list[$id];

            $movie['rating'] = 0;
            $movie['seen'] = null;
            $movie['status'] = 0;
            $movie['score'] = floatval($movieData[6]);

            $g = false;

            $database->update('movies', $movie, ['id' => $movie['id']]);
        } else {
            $movie = [
                'title' => $movieData[5],
                'imdb_id' => $movieData[1],
                'rating' => 0,
                'seen' => $movieData[3],
                'status' => 0,
                'score' => floatval($movieData[8]),
                'year' => intval($movieData[10]),
            ];

            $g = true;

            $database->insert('movies', $movie);
            $movie['id'] = $database->id();
        }
    }

    $forceCover = false;

    if (!in_array('gallery/movies/' . $movie['id'] . '.jpg', $existingImages)) {
        $forceCover = true;
        logStatus("Missing: " . $movie['id'] . " - " . $movie['title'], 'update');
    } else {
        logStatus("Exists: " . $movie['id'] . " - " . $movie['title'], 'process');
    }

    if (($g || isset($_GET['reuploadImages'])) || $forceCover) {
        getMore($movie, $database, $SesClient);
    }
}

function getMore($movie, $database, $SesClient) {
    $info = file_get_contents('http://www.omdbapi.com/?i=' . $movie['imdb_id'] . '&apikey=' . getenv('OMDB_API_KEY'));
    $info = json_decode($info, true);

    if (isset($info['Title'])) {
        $movie['description'] = $info['Plot'];

        $database->update('movies', $movie, ['id' => $movie['id']]);

        if ($info['Poster'] !== 'N/A') {
            downloadImage($SesClient, $info['Poster'], $movie['id'], 'anime', 'gallery/movies/', $movie['title'], 250);
        }
    }
}

function importcsv($database, $SesClient) {
    logStatus("Start movie import", 'title');

    $existingImages = getExistingImages($SesClient, 'gallery/movies/');

    if ($_FILES['csv'] && $_FILES['csv']['tmp_name']) {
        $csv = file_get_contents($_FILES['csv']['tmp_name']);

        $csv_x = explode ("\n", $csv);

        $list = $database->select('movies', ['id', 'imdb_id']);

        foreach ($csv_x as $key => $item) {
            if ($key > 0) {
                addOrUpdate($item, $list, $database, $SesClient, $existingImages);
            }
        }

        echo "Done!";
    }
}
