<?php

use Medoo\Medoo;

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

require_once "cors.php";

cors();

set_time_limit(3600);
ini_set('max_execution_time', 3600);

require 'Medoo.php';

$pdo = new PDO('mysql:host=' . getenv('MYSQL_HOST') . ';dbname=' . getenv('MYSQL_DB'), getenv('MYSQL_USER'), getenv('MYSQL_PASS'));

$database = new Medoo([
    // Initialized and connected PDO object
    'pdo' => $pdo,
    // [optional] Medoo will have different handle method according to different database type
    'database_type' => 'mysql'
]);

$database->query("SET NAMES 'utf8'")->fetchAll();

function importdi($db)
{
    var_dump($_POST);
    var_dump($_REQUEST);
}

importdi($database);
