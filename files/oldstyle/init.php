<?php

use Aws\S3\S3Client;
use Medoo\Medoo;

require '../lib/vendor/autoload.php';

set_time_limit(3600);
ini_set('max_execution_time', 3600);

require 'Medoo.php';

require 'log.php';
require 'downloadImage.php';

function curl($url, $simulate_local = null)
{
    if ($simulate_local) {
        return file_get_contents($simulate_local);
    }

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $output = curl_exec($ch);

    curl_close($ch);

    return $output;
}