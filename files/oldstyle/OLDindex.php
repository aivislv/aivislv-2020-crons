<?php
require '../lib/vendor/autoload.php';

require 'Medoo.php';



$database = new Medoo([
    // Initialized and connected PDO object
    'pdo' => $pdo,
    // [optional] Medoo will have different handle method according to different database type
    'database_type' => 'mysql'
]);

$data = $database->select('email_response_log', ['[>]message'=>['message_id'=>'id'], '[>]backers'=>['message.backer_id'=>'id']], ['message_id', 'email_response_log.id', 'email', 'title', 'body'], ['status'=>0]);

//  use Aws\S3\S3Client;
use Aws\Ses\SesClient;
use Medoo\Medoo;

$SesClient = new SesClient([
    'version' => '2010-12-01',
    'region'  => 'eu-west-1',
    'credentials' => [
        'key'=> getenv('AWS_ACCESS_KEY_ID'), //'AKIAVPFDGBCY3ZRE5UXC',
        'secret' => getenv('AWS_SECRET_ACCESS_KEY'), //'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',
    ]
]);

$sender_email = 'spam@aivis.lv';

$recipient_emails = ['spam@aiviszzzzzzzzzzzzzzz.lv'];

foreach($data as $item) {
    // var_dump($item);
    try {
        $result = $SesClient->sendEmail([
            'Destination' => [
                'ToAddresses' => $recipient_emails,
            ],
            'ReplyToAddresses' => [$sender_email],
            'Source' => $sender_email,
            'Message' => [
                'Body' => [
                    'Html' => [
                        'Charset' => 'UTF-8',
                        'Data' => $item['body'],
                    ],
                    'Text' => [
                        'Charset' => 'UTF-8',
                        'Data' => strip_tags($item['body']),
                    ],
                ],
                'Subject' => [
                    'Charset' => 'UTF-8',
                    'Data' => $item['title'],
                ],
            ],
        ]);
        $messageId = $result['MessageId'];

        var_dump($result);

        $database->update('email_response_log', ['status'=>1, 'aws_id'=>$messageId], ['id'=>$item['id']]);

        echo("Email sent! Message ID: $messageId"."\n");
    } catch (AwsException $e) {
        // output error message if fails
        echo $e->getMessage();
        echo("The email was not sent. Error message: ".$e->getAwsErrorMessage()."\n");
        echo "\n";
    }
}
