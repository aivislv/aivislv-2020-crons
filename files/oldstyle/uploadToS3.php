<?php
set_time_limit ( 3600 );
ini_set('max_execution_time',3600);

require '../lib/vendor/autoload.php';

require 'Medoo.php';

$pdo = new PDO('mysql:host=' . getenv('MYSQL_HOST') . ';dbname=' . getenv('MYSQL_DB'), getenv('MYSQL_USER'), getenv('MYSQL_PASS'));

$database = new Medoo([
    // Initialized and connected PDO object
    'pdo' => $pdo,
    // [optional] Medoo will have different handle method according to different database type
    'database_type' => 'mysql'
]);

$data = $database->select('images', ['id', 'gid', 'ext', 'width', 'height', 'title'], ['link' => '']);

use Aws\S3\S3Client;
use Medoo\Medoo;

// use Aws\Ses\SesClient;

$SesClient = new S3Client([
    'version' => 'latest',
    'region' => getenv('AWS_REGION'),
    'credentials' => [
        'key' => getenv('AWS_ACCESS_KEY_ID'),
        'secret' => getenv('AWS_SECRET_ACCESS_KEY'),
    ]
]);

$sizes = [2560, 1920, 1280, 1024];
$thumbSizes = [500, 250];

foreach($data as $item) {
    try {
        $realPath = getcwd() . '/images/' . $item['gid'] . '/' . $item['id'] . '.' . $item['ext'];

        $SesClient->putObject([
            'Bucket' => 'cdn.aivis.lv',
            'Key'    => 'photos_originals/' . $item['id'] . '.' . $item['ext'],
            'Body'   => fopen($realPath, 'r'),
            'ACL'    => 'public-read',
        ]);

        $imagick = new Imagick(realpath($realPath));
        $imageSizes = $imagick->getImageGeometry();
        $imageProportions = $imageSizes['width']/$imageSizes['height'];

        foreach($sizes as $size) {
            $imagick = new Imagick(realpath($realPath));

            if ($imageProportions > 1) {
                $newWidth = $size;
                $newHeight = round($size / $imageProportions);
            } else {
                $newWidth = round($size * $imageProportions);
                $newHeight = $size;
            }

            $newWidth = $imageSizes['width'] > $newWidth ? $newWidth : $imageSizes['width'];
            $newHeight = $imageSizes['height'] > $newHeight ? $newHeight : $imageSizes['height'];

            $imagick->resizeImage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1);

            $saveTmp = getcwd() . '/images/tmp/' . $size . '/' . $item['id'] .'.'. $item['ext'];

            $imagick->writeImage($saveTmp);

            $SesClient->putObject([
                'Bucket' => 'cdn.aivis.lv',
                'Key'    => 'gallery/' . $size . '/' . $item['id'] . '.' . $item['ext'],
                'Body'   => fopen($saveTmp, 'r'),
                'ACL'    => 'public-read',
            ]);
        }

        foreach($thumbSizes as $size) {
            $imagick = new Imagick(realpath($realPath));
            if ($imageProportions < 1) {
                $newWidth = $size;
                $newHeight = round($size / $imageProportions);
                $x = 0;
                $y = ($newHeight - $size) / 2;
            } else {
                $newWidth = round($size * $imageProportions);
                $newHeight = $size;
                $y = 0;
                $x = ($newWidth - $size) / 2;
            }

            $imagick->resizeImage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1);

            $imagick->cropImage($size, $size, $x, $y);

            $saveTmp = getcwd() . '/images/tmp/' . $size . '/' . $item['id'] .'.'. $item['ext'];

            $imagick->writeImage($saveTmp);

            $SesClient->putObject([
                'Bucket' => 'cdn.aivis.lv',
                'Key'    => 'gallery/' . $size . '/' . $item['id'] . '.' . $item['ext'],
                'Body'   => fopen($saveTmp, 'r'),
                'ACL'    => 'public-read',
            ]);

        }

        $data = $database->update('images', ['link' => 'x'], ['id'=>$item['id']]);

    } catch (AwsException $e) {
        // output error message if fails
        echo $e->getMessage();
        echo("The email was not sent. Error message: ".$e->getAwsErrorMessage()."\n");
        echo "\n";
    }
}