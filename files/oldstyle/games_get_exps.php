<?php

require 'init.php';

import($database, $SesClient);

function import($database, $SesClient)
{
    $filters = ["type" => 1, "expansion" => 0];

    if (!isset($_GET['all'])) {
        $filters["skip_exp_check"] = 0;
    }

    if (isset($_GET['id'])) {
        $filters["id"] = intval($_GET['id']);
    }

    $game_list = $database->select("games", ["id", "bgg_id"], $filters);

    foreach ($game_list as $game) {
        importGame($database, $game);
        sleep(3);
    }
}

function importGame($database, $game)
{
    $xml = file_get_contents('https://www.boardgamegeek.com/xmlapi2/thing?type=boardgame,boardgameexpansion,boardgameaccessory&id=' . $game['bgg_id']);
    $docs = new SimpleXMLElement($xml);
    $json = json_encode($docs);
    $array = json_decode($json, TRUE);

    $expansionlist = $array['item']['link'];

    foreach ($expansionlist as $expansion) {
        if (processExpansion($database, $expansion, $game['id'])) {
            sleep(3);
        }
    }
}

function processExpansion($database, $expansion, $parent_id)
{
    if ($expansion['@attributes']['type'] == 'boardgameexpansion' || $expansion['@attributes']['type'] == 'boardgameaccessory' || $expansion['@attributes']['type'] == 'boardgameintegration') {
        $foundExpansions = ['id' => $expansion['@attributes']['id'], 'title' => $expansion['@attributes']['value']];

        $already_id_db = $database->get("games", ["id"], ["bgg_id" => $foundExpansions['id']]);
        if ($already_id_db) {
            return false;
        }

        $already_processed = $database->get("game_expansion_importer", ["id"], ["bgg_id" => $foundExpansions['id']]);
        if ($already_processed) {
            return false;
        }

        $xml = file_get_contents('https://www.boardgamegeek.com/xmlapi2/thing?type=boardgame,boardgameexpansion,boardgameaccessory&id=' . $foundExpansions['id']);
        $docs = new SimpleXMLElement($xml);
        $json = json_encode($docs);
        $array = json_decode($json, TRUE);

        $spread = [
            'boardgameexpansion' => 'expansion',
            'boardgameaccessory' => 'accessory',
            'boardgameintegration' => 'integration'
        ];


        $database->insert("game_expansion_importer", [
            "bgg_id" => $foundExpansions['id'],
            "parent_id" => $parent_id,
            "title" => $foundExpansions['title'],
            'type' => $spread[$array['item']['@attributes']['type']],
            "image_url" => trim($array['item']['image']),
            "description" => trim($array['item']['description']),
        ]);

        return true;
    }

    return false;
}