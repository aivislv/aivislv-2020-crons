<?php
require 'init.php';

import($database, $SesClient);

function extractData($animes, $database, $SesClient, $existingImages) {
    $status = [
        'watching' => 1,
        'completed' => 2,
        'on_hold' => 3,
        'dropped' => 4,
        'plan_to_watch' => 6,
    ];

    foreach ($animes['data'] as $an) {

        $anime = $database->get('animes', ['id', 'mal_id'], ['mal_id' => intval($an['node']['id'])]);

        if (!$anime) {
            logStatus("Adding: <strong>" . $an['node']['title'] . "</strong>", 'update');

            $anime = [];

            $anime['mal_id'] = intval($an['node']['id']);
            $anime['rating'] = intval($an["list_status"]['score']);
            $anime['title'] = $an['node']['title'];
            $anime['status'] = $status[$an["list_status"]['status']];
            $anime['image'] = $an['node']['main_picture']['large'];
            $anime['updated'] = intval(time());

            $g = true;

            $database->insert('animes', $anime);
            $anime['id'] = $database->id();

        } else {
            logStatus("Updating: <strong>" . $an['title'] . "</strong>", 'process');

            $anime['rating'] = intval($an["list_status"]['score']);
            $anime['status'] = $status[$an["list_status"]['status']];

            $g = false;

            $database->update('animes', $anime, ['id' => $anime['id']]);
        }

        $forceCover = false;

        if (!in_array('gallery/anime/' . $anime['id'] . '.jpg', $existingImages)) {
            $forceCover = true;
            logStatus("Missing: " . $anime['id'] . " - " . $anime['title'], 'update');
        } else {
            logStatus("Exists: " . $anime['id'] . " - " . $anime['title'], 'process');
        }

        if ($g || isset($_GET['reuploadImages']) || $forceCover) {
            downloadImage($SesClient, $an['node']['main_picture']['large'], $anime['id'], 'anime', 'gallery/anime/', $anime['title'], 250);
        }
    }
}

function import($database, $SesClient)
{
    $opts = [
        "http" => [
            "method" => "GET",
            "header" => "X-MAL-CLIENT-ID: " . getenv('MAL_ID') . "\r\n"
        ]
    ];

    $context = stream_context_create($opts);

    $page = 0;
    $limit = 1000;
    $ok = true;

    logStatus("<a href='" . getenv('ADMIN_HOST') . "/crons'>Back to Import</a>", 'title');
    logStatus("<h3>Anime</h3>", 'title');

    $existingImages = getExistingImages($SesClient, 'gallery/anime/');

    while ($ok) {
        $json = file_get_contents('https://api.myanimelist.net/v2/users/aivislisovskis/animelist?fields=list_status&limit=' . $limit . '&offset=' . ($page * $limit), false, $context);

        $animes = json_decode($json, true);

        if (count($animes['data']) > 0) {
            extractData($animes, $database, $SesClient, $existingImages);
            $page++;
        } else {
            $ok = false;
        }
    }
}