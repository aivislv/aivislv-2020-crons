<?php
require 'init.php';

import($database, $SesClient);

function extractData($animes, $database, $SesClient, $existingImages)
{
    foreach ($animes['anime'] as $an) {
        $anime = $database->get('animes', ['id', 'mal_id'], ['mal_id' => intval($an['mal_id'])]);

        if (!$anime) {
            logStatus("Adding: <strong>" . $an['title'] . "</strong>", 'update');

            $anime = [];

            $anime['mal_id'] = intval($an['mal_id']);
            $anime['rating'] = intval($an['score']);
            $anime['title'] = $an['title'];
            $anime['status'] = intval($an['watching_status']);
            $anime['image'] = intval($an['image_url']);
            $anime['updated'] = intval(time());

            $g = true;

            $database->insert('animes', $anime);
            $anime['id'] = $database->id();

        } else {
            logStatus("Updating: <strong>" . $an['title'] . "</strong>", 'process');

            $anime['rating'] = intval($an['score']);
            $anime['status'] = $an['watching_status'];

            $g = false;

            $database->update('animes', $anime, ['id' => $anime['id']]);
        }

        $forceCover = false;

        if (!in_array('gallery/anime/' . $anime['id'] . '.jpg', $existingImages)) {
            $forceCover = true;
            logStatus("Missing: " . $anime['id'] . " - " . $anime['title'], 'update');
        } else {
            logStatus("Exists: " . $anime['id'] . " - " . $anime['title'], 'process');
        }

        if ($g || isset($_GET['reuploadImages']) || $forceCover) {
            downloadImage($SesClient, $an['image_url'], $anime['id'], 'anime', 'gallery/anime/', $anime['title'], 250);
        }
    }
}

function import($database, $SesClient)
{
    $page = 1;
    $ok = true;

    logStatus("<a href='" . getenv('ADMIN_HOST') . "/crons'>Back to Import</a>", 'title');
    logStatus("<h3>Anime</h3>", 'title');

    $existingImages = getExistingImages($SesClient, 'gallery/anime/');

    while ($ok) {
        $json = file_get_contents('https://api.jikan.moe/v3/user/aivislisovskis/animelist?page=' . $page);
        $animes = json_decode($json, true);

        if (count($animes['anime']) > 0) {
            extractData($animes, $database, $SesClient, $existingImages);
            $page++;
        } else {
            $ok = false;
        }
    }
}