<?php

///
///
///  DEPRECATED; All scripts moved to games.php
///
///
///
require 'init.php';

import($database);

function import($database)
{
    $baseGames = $database
        ->query("SELECT `id`, `bgg_id`, `expansion`, `players` FROM `games` WHERE `expansion` = 0")
        ->fetchALl();

    foreach ($baseGames as $game) {
        if ($game['bgg_id'] > 0 && $game['expansion'] == 0 && $game['players'] === "") {
            doImportBest($database, $game);
        }
    }
}

function doImportBest($database, $game)
{
    $voteType = [
        "Best" => 1,
        "Recommended" => 2,
        "Not Recommended" => 3,
    ];

    libxml_use_internal_errors(true);

    try {
        $xml = file_get_contents('https://www.boardgamegeek.com/xmlapi/boardgame/' . $game['bgg_id']);

        $docs = new SimpleXMLElement($xml);
        $json = json_encode($docs);
        $array = json_decode($json, TRUE);

        $best = null;

        foreach ($array['boardgame']['poll'] as $poll) {
            if ($poll['@attributes']['name'] == "suggested_numplayers") {
                foreach ($poll['results'] as $item) {

                    $res = [];
                    foreach ($item['result'] as $result) {
                        $res[$result['@attributes']['value']] = $result['@attributes']['numvotes'];
                    }

                    $maxT = "Not Recommended";
                    $maxV = 0;

                    foreach ($res as $type => $votes) {
                        if ($votes > $maxV) {
                            $maxT = $type;
                            $maxV = $votes;
                        }
                    }

                    $best[] = [
                        'players' => $item['@attributes']['numplayers'],
                        'votes' => $maxV,
                        'status' => $voteType[$maxT],
                    ];
                }
            }
        }

        $database->update('games', ['players' => json_encode($best)], ['id' => $game['id']]);

        foreach ($best as $item) {
            $players = $database->get('games_players', ['game_id' => $game['id'], 'players' => $item['players']]);
            if ($players) {
                $players['votes'] = $item['votes'];
                $players['status'] = $item['status'];

                $database->update('games_players', $players, ['id' => $players['id']]);
            } else {
                $players = ['players' => $item['players'], 'game_id' => $game['id']];
                $players['votes'] = $item['votes'];
                $players['status'] = $item['status'];
                $database->insert('games_players', $players);
            }
        }
    } catch (Exception $e) {
        logStatus("No Data", 'error');
        var_dump($e);
    }
}