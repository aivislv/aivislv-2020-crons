FROM php:7.2.13-apache

WORKDIR /var/www/html

RUN apt-get --allow-unauthenticated update && \
    apt-get --allow-unauthenticated upgrade -y && \
    apt-get --allow-unauthenticated install -y git
RUN apt-get update && apt-get install -y zlib1g-dev
RUN apt-get update && apt-get install -y libmemcached11
RUN apt-get update && apt-get install -y libmemcached-dev
RUN apt-get install -y zip
RUN curl -sS 'https://getcomposer.org/installer' | php
RUN mv 'composer.phar' '/usr/local/bin/composer'

RUN docker-php-ext-configure mysqli --with-mysqli
RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql
RUN docker-php-ext-install pdo pdo_mysql mysqli iconv

RUN apt-get install -y libmagickwand-dev --no-install-recommends && rm -rf /var/lib/apt/lists/
RUN pecl install imagick
RUN docker-php-ext-enable imagick

RUN pecl install memcached-3.1.5
RUN pecl install memcache-4.0.5.2

RUN echo extension=memcached.so >> /usr/local/etc/php/conf.d/memcached.ini
RUN echo extension=memcache.so >> /usr/local/etc/php/conf.d/memcache.ini

# update composer installation
RUN mkdir -p /var/www/lib

COPY ./composer.json /var/www/lib/composer.json
WORKDIR /var/www/lib
RUN composer update

WORKDIR /var/www/

EXPOSE 80
